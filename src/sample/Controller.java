package sample;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

//import javafx.scene.image.Image;


public class Controller implements Initializable {
    public Button btnWork;
    public ProgressBar pbWorking;
    public ImageView iv;
    private Timeline timeline;

    public void onWorking(ActionEvent actionEvent) {
        //pbWorking.setVisible(true);
        //timeline.play();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pbWorking.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        pbWorking.setVisible(false);


        List<String> imagepath = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            imagepath.add("/res/".concat(String.valueOf(i)).concat(".png"));
        }

        /*

        timeline = new Timeline(
                new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        iv.setImage(new Image(getClass().getResource(imagepath.get(1)).toString()));
                    }
                }),
                new KeyFrame(Duration.seconds(2), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        iv.setImage(new Image(getClass().getResource(imagepath.get(2)).toString()));
                    }
                }),
                new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        iv.setImage(new Image(getClass().getResource(imagepath.get(3)).toString()));
                    }
                }),
                new KeyFrame(Duration.seconds(4), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        iv.setImage(new Image(getClass().getResource(imagepath.get(4)).toString()));
                    }
                })
        );
        */

        timeline = new Timeline();
        /*
        for (int i = 0; i < 5; i++) {
            final int j = i;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(j + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            iv.setImage(new Image(getClass().getResource(imagepath.get(j)).toString()));
                        }
                    })
            );
        }
        */


        IntStream.range(0, imagepath.size())
                .forEach(e -> {
                    timeline.getKeyFrames().add(
                            new KeyFrame(Duration.seconds(e + 1), new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    iv.setImage(new Image(getClass().getResource(imagepath.get(e)).toString()));
                                }
                            })
                    );
                });

        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);
//timeline.play();

    }
}
